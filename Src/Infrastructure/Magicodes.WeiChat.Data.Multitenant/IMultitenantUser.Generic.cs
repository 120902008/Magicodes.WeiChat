﻿using Microsoft.AspNet.Identity;

namespace Magicodes.WeiChat.Data.Multitenant
{
    /// <summary>
    ///     定义多租户接口
    /// </summary>
    /// <typeparam name="TKey">用户Id类型 <see cref="IUser.Id" /> </typeparam>
    /// <typeparam name="TTenantKey">租户Id类型<see cref="TenantId" /></typeparam>
    public interface IMultitenantUser<out TKey, TTenantKey> : IUser<TKey>
    {
        /// <summary>
        ///     多租户Id
        /// </summary>
        TTenantKey TenantId { get; set; }
    }
}