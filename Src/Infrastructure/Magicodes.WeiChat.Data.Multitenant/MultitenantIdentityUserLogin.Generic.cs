﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Magicodes.WeiChat.Data.Multitenant
{
    /// <summary>
    ///     多租户UserLogin定义
    /// </summary>
    /// <typeparam name="TKey"> <see cref="IdentityUserLogin.UserId" /> 类型</typeparam>
    /// <typeparam name="TTenantKey"><see cref="TenantId" /> 类型</typeparam>
    public class MultitenantIdentityUserLogin<TKey, TTenantKey> : IdentityUserLogin<TKey>
    {
        /// <summary>
        ///     获取或设置多租户唯一标示Id.
        /// </summary>
        public virtual TTenantKey TenantId { get; set; }
    }
}