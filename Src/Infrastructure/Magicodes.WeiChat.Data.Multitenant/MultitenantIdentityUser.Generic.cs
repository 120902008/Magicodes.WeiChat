﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Magicodes.WeiChat.Data.Multitenant
{
    /// <summary>
    ///     多租户用户类
    /// </summary>
    /// <typeparam name="TKey">用户Id类型： <see cref="IUser.Id" /> </typeparam>
    /// <typeparam name="TTenantKey">租户Id类型： <see cref="IMultitenantUser{TKey, TTenantKey}.TenantId" /> </typeparam>
    /// <typeparam name="TLogin">用户登录类型</typeparam>
    /// <typeparam name="TRole">用户角色类型</typeparam>
    /// <typeparam name="TClaim">User claim类型</typeparam>
    public class MultitenantIdentityUser<TKey, TTenantKey, TLogin, TRole, TClaim>
        : IdentityUser<TKey, TLogin, TRole, TClaim>, IMultitenantUser<TKey, TTenantKey>
        where TLogin : MultitenantIdentityUserLogin<TKey, TTenantKey>
        where TRole : IdentityUserRole<TKey>
        where TClaim : IdentityUserClaim<TKey>
    {
        /// <summary>
        /// 租户Id
        /// </summary>
        [Display(Name = "租户")]
        public TTenantKey TenantId { get; set; }
    }
}