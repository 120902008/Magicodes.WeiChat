﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magicodes.WeiChat.Unity;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Framework.Apis.TemplateMessage;
using Magicodes.WeiChat.Data.Models.WeiChat;
using Magicodes.WeiChat.Infrastructure;

namespace Magicodes.WeiChat.WeChatHelper.Task
{
    /// <summary>
    /// 任务管理
    /// </summary>
    public class TaskManager : ThreadSafeLazyBaseSingleton<TaskManager>
    {
        public void StartAllTasks()
        {
            RegisterFuncs();
        }
        public void RegisterFuncs()
        {
            //注册获取配置函数：根据Key获取微信配置（加载一次后续将缓存）
            WeiChatFrameworkFuncsManager.Current.Register(WeiChatFrameworkFuncTypes.Config_GetWeiChatConfigByKey,
                (key) =>
                {
                    using (AppDbContext _db = new AppDbContext())
                    {
                        var appConfig = _db.WeiChat_Apps.Find(key);
                        if (appConfig == null)
                        {
                            throw new Exception("您尚未配置公众号，请配置公众号信息！");
                        }
                        return appConfig;
                    }
                });
            //模板消息发送:记录发送日志
            WeiChatFrameworkFuncsManager.Current.Register(WeiChatFrameworkFuncTypes.APIFunc_TemplateMessageApi_Create,
                (model) =>
                {
                    var messagesTemplateLogFuncModel = model as List<MessagesTemplateLogFuncModel>;
                    if (messagesTemplateLogFuncModel == null || messagesTemplateLogFuncModel.Count == 0)
                    {
                        return true;
                    }
                    var tenantId = WeiChatApplicationContext.Current.TenantId;
                    var userId = WeiChatApplicationContext.Current.UserId;
                    var messageTplNo = messagesTemplateLogFuncModel.First().MessagesTemplateNo;
                    using (AppDbContext _db = new AppDbContext())
                    {
                        var messageTemplate = _db.WeiChat_MessagesTemplates.FirstOrDefault(p => p.TenantId == tenantId && p.TemplateNo == messageTplNo);
                        if (messageTemplate == null)
                        {
                            throw new Exception(string.Format("模板库中不存在此模板消息，请先维护。模板Id：{0}。模板库地址：https://mp.weixin.qq.com/advanced/tmplmsg?action=tmpl_store&t=tmplmsg/store&token=1241910100&lang=zh_CN", messageTplNo));
                        }
                        var logs = messagesTemplateLogFuncModel.Select(p => new WeiChat_MessagesTemplateSendLog()
                        {
                            CreateTime = p.CreateTime,
                            BatchNumber = p.BatchNumber,
                            Content = p.Content,
                            MessagesTemplateNo = p.MessagesTemplateNo,
                            MessagesTemplateId = messageTemplate.Id,
                            ReceiverId = p.ReceiverId,
                            TopColor = p.TopColor,
                            Url = p.Url,
                            TenantId = tenantId,
                            CreateBy = userId,
                            Result = p.Result
                        }).ToList();
                        _db.WeiChat_MessagesTemplateSendLogs.AddRange(logs);
                        _db.SaveChanges();
                    }
                    return true;
                });
        }
    }
}
