﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magicodes.WeiChat.Infrastructure.Config
{
    public abstract class ConfigBase
    {
        public ConfigBase()
        {
            UpdateTime = DateTime.Now;
        }
        public DateTime UpdateTime { get; set; }
    }
}
