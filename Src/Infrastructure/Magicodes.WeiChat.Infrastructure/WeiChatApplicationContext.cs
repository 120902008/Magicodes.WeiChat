﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Filters;
using Senparc.Weixin.MP.CommonAPIs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Magicodes.WeiChat.Data;
using Microsoft.Owin;
using Magicodes.WeiChat.Infrastructure.Identity;
using Microsoft.AspNet.Identity;
using NLog;
using Magicodes.WeiChat.Unity;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Infrastructure
{
    /// <summary>
    /// 微信全局上下文对象
    /// </summary>
    public class WeiChatApplicationContext : ThreadSafeLazyBaseSingleton<WeiChatApplicationContext>
    {
        internal const string UserSessionName = "Magicodes.Weichat_User";
        internal const string TenantIdSessionName = "Magicodes.TenantId";
        internal const string WeiChatContextSessionName = "Magicodes.WeiChatContext";
        /// <summary>
        /// OpenId的Cookie名称
        /// </summary>
        internal const string OpenIdCookieName = "Magicodes.Weichat_OpenId";

        /// <summary>
        /// 微信AppId
        /// </summary>
        public string AppId { get { return WeiChatConfigManager.Current.AppId; } }
        /// <summary>
        /// 接口访问密钥
        /// </summary>
        public string AppSecret { get { return WeiChatConfigManager.Current.AppSecret; } }

        public string UserId { get { return HttpContext.Current.User.Identity.GetUserId(); } }

        /// <summary>
        /// 租户Id（如果是系统租户则能获取到参数中的租户Id）
        /// </summary>
        public int TenantId
        {
            get
            {
                //租户Id
                int tenantId = default(int);
                //请求参数中的租户Id
                var reqTennantId = default(int);

                #region 获取请求参数中的租户Id
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.Request.QueryString["TenantId"]))
                {
                    reqTennantId = Convert.ToInt32(HttpContext.Current.Request.QueryString["TenantId"]);
                }
                else
                {
                    reqTennantId = HttpContext.Current.Request.RequestContext.RouteData.Values["TenantId"] != null ? Convert.ToInt32(HttpContext.Current.Request.RequestContext.RouteData.Values["TenantId"]) : default(int);
                }
                #endregion

                //通过Session获取租户Id
                if (HttpContext.Current != null && HttpContext.Current.Session[TenantIdSessionName] != null)
                {
                    tenantId = Convert.ToInt32(HttpContext.Current.Session[TenantIdSessionName]);
                }
                //代理租户Id
                var agentTennantId = default(int);

                if (tenantId == default(int) || (tenantId != reqTennantId && reqTennantId != default(int)))
                {
                    using (var db = new AppDbContext())
                    {
                        var user = db.Users.Find(UserId);
                        //根据登录角色获取
                        tenantId = user.TenantId;
                        agentTennantId = user.AgentTennantId;

                        //如果当前租户为系统租户，则允许代理其他租户操作
                        if (db.Account_Tenants.Any(p => p.Id == tenantId && p.IsSystemTenant))
                        {
                            //系统租户使用AgentTennantId
                            if (user.AgentTennantId != default(int) && user.AgentTennantId != tenantId)
                                tenantId = user.AgentTennantId;

                            //如果当前租户Id与请求租户Id不一致
                            if (tenantId != reqTennantId && reqTennantId != default(int))
                            {
                                user.AgentTennantId = reqTennantId;
                                db.SaveChanges();
                                tenantId = reqTennantId;
                            }
                        }
                    }
                }
                if (tenantId == default(int))
                {
                    throw new Exception("无法获取到租户Id！");
                }
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Session[TenantIdSessionName] = tenantId;
                }
                return tenantId;
            }
        }
        /// <summary>
        /// 租户信息
        /// </summary>
        public ITenant<int> TenantInfo
        {
            get
            {
                using (var db = new AppDbContext())
                {
                    return db.Account_Tenants.Find(TenantId);
                }
            }
        }
        /// <summary>
        /// 获取微信用户信息
        /// </summary>
        public WeiChat_User WeiChatUser
        {
            get
            {
                var log = LogManager.GetCurrentClassLogger();
                if (HttpContext.Current.Session[UserSessionName] != null)
                {
                    log.Trace("Session is not Null");
                    return HttpContext.Current.Session[UserSessionName] as WeiChat_User;
                }
                if (HttpContext.Current.Items[UserSessionName] != null)
                {
                    log.Trace("Items is not Null");
                    var user = HttpContext.Current.Items[UserSessionName] as WeiChat_User;
                    HttpContext.Current.Session[UserSessionName] = user;
                    return user;
                }
                var openIdCookie = new HttpCookie(OpenIdCookieName);
                if (openIdCookie != null && !string.IsNullOrEmpty(openIdCookie.Value))
                {
                    log.Trace("openIdCookie is not Null");
                    using (var db = new AppDbContext())
                    {
                        var user = db.WeiChat_Users.FirstOrDefault(p => p.OpenId == openIdCookie.Value);
                        HttpContext.Current.Session[UserSessionName] = user;
                        return user;
                    }
                }
                //if (HttpContext.Current.User.Identity.IsAuthenticated && AppUser != null)
                //{
                //    using (AppDbContext db = new AppDbContext())
                //    {
                //        return db.WeiChat_Users.FirstOrDefault(p => p.OpenId == AppUser.OpenId);
                //    }
                //}
                return null;
            }
        }
    }
}
