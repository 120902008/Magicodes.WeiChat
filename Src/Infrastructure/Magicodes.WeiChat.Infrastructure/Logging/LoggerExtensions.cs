﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
namespace Magicodes.WeiChat.Infrastructure.Logging
{
    public static class LoggerExtensions
    {
        public static void LogException(this Logger logger, Exception exception)
        {
            logger.Error(exception);
            if (exception is AggregateException && exception.InnerException != null)
            {
                var aggException = exception as AggregateException;
                if (aggException.InnerException is DbEntityValidationException)
                {
                    var sb = new StringBuilder();
                    var validationException = aggException.InnerException as DbEntityValidationException;
                    sb.AppendLine("实体验证错误。错误数：" + validationException.EntityValidationErrors.Count() + " ");
                    foreach (var validationResult in validationException.EntityValidationErrors)
                    {
                        foreach (var item in validationResult.ValidationErrors)
                        {
                            sb.AppendFormat("（{0}:{1}）", item.PropertyName, item.ErrorMessage);
                        }
                    }
                    logger.Error(sb.ToString());
                }
                else if (exception is DbEntityValidationException)
                {
                    var sb = new StringBuilder();
                    var validationException = exception as DbEntityValidationException;
                    sb.AppendLine("实体验证错误。错误数：" + validationException.EntityValidationErrors.Count() + " ");
                    foreach (var validationResult in validationException.EntityValidationErrors)
                    {
                        foreach (var item in validationResult.ValidationErrors)
                        {
                            sb.AppendFormat("（{0}:{1}）", item.PropertyName, item.ErrorMessage);
                        }
                    }
                    logger.Error(sb.ToString());
                }
            }
        }
    }
}
