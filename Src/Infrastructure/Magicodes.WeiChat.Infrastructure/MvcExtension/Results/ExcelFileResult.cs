﻿//using ClosedXML.Excel;
//using CsvHelper;
//using CsvHelper.Excel;
//using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Infrastructure.MvcExtension.Results
{
    //https://github.com/christophano/CsvHelper.Excel
    public class ExcelFileResult<T> : FileResult where T : class
    {
        private IEnumerable<T> _data;


        public ExcelFileResult(IEnumerable<T> data)
            : base("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        {
            _data = data;
        }


        protected override void WriteFile(HttpResponseBase response)
        {
            //var outPutStream = response.OutputStream;
            //using (var streamWriter = new StreamWriter(outPutStream, Encoding.UTF8))
            //{
            //    using (var workbook = new XLWorkbook(outPutStream, XLEventTracking.Disabled))
            //    {
            //        using (var writer = new CsvWriter(new ExcelSerializer(workbook)))
            //        {
            //            writer.WriteHeader<T>();
            //            foreach (var item in _data)
            //            {
            //                writer.WriteRecord(item);
            //            }
            //            streamWriter.Flush();
            //            response.Flush();
            //        }
            //    }


            //}

        }
    }
}
