﻿using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Framework.Apis.User;
using Magicodes.WeiChat.Infrastructure.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using NLog;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Infrastructure.MvcExtension.Filters
{
    /// <summary>
    /// 网页授权获取用户基本信息特性
    /// 关于公众号如何获取用户信息，请参考此文档：http://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class WeChatOAuthAttribute : FilterAttribute, IAuthorizationFilter
    {
        private string _state = "magicodes.weichat";
        const string RedirectUrlCookieName = "Magicodes.Weichat_RedirectUrlCookie";
        Logger logger = LogManager.GetCurrentClassLogger();

        public WeChatOAuthAttribute()
        {
        }
        public WeChatOAuthAttribute(string state)
        {
            _state = state;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            HttpContextBase httpContextBase = filterContext.HttpContext;
            HttpRequestBase request = httpContextBase.Request;
            logger.Trace("正在获取授权...{0}", request.Url.ToString());
            //如果用户已经验证或者已经获取微信信息，则不再验证（提高效率）
            if (WeiChatApplicationContext.Current.WeiChatUser != null)
            {
                logger.Trace("用户已经验证或者已经获取微信信息，不再验证...{0}", request.Url.ToString());
                return;
            }

            //用户的OPENID
            HttpCookie openIdCookie = request.Cookies[WeiChatApplicationContext.OpenIdCookieName];
            var code = request.QueryString["code"];
            var state = request.QueryString["state"];
            #region 如果是从微信验证页面跳转回来
            if ((!string.IsNullOrEmpty(code)) && (!string.IsNullOrEmpty(state)))
            {
                logger.Trace("从微信验证页面跳转回来...{2}\ncode:{0}\tstate:{1}", code, state, request.Url.ToString());
                //TODO:判断是否来自open.weixin.qq.com/connect/oauth2/authorize
                //if (request.)
                //{

                //}
                //通过code换取access_token,Code只能用一次
                //网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
                var result = OAuthApi.GetAccessToken(WeiChatApplicationContext.Current.AppId, WeiChatApplicationContext.Current.AppSecret, code);

                openIdCookie = new HttpCookie(WeiChatApplicationContext.OpenIdCookieName);
                openIdCookie.Value = result.openid;
                httpContextBase.Response.Cookies.Add(openIdCookie);
                #region 获取微信用户信息
                var userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                WeiChat_User user;
                try
                {
                    using (var db = new AppDbContext())
                    {
                        user = db.WeiChat_Users.FirstOrDefault(p => p.OpenId == userInfo.openid);
                        if (user == null)
                        {
                            user = new WeiChat_User()
                            {
                                City = userInfo.city,
                                Country = userInfo.country,
                                //GroupId = userInfo.groupid,
                                HeadImgUrl = userInfo.headimgurl,
                                //Language = userInfo.language,
                                NickName = userInfo.nickname,
                                OpenId = userInfo.openid,
                                Province = userInfo.province,
                                //Remark = userInfo.remark,
                                Sex = (WeChatSexTypes)userInfo.sex,
                                Subscribe = false,
                                SubscribeTime = DateTime.Now,
                                UnionId = userInfo.unionid,
                                //TenantId=
                            };
                            //db.WeiChat_Users.Add(user);
                            //db.SaveChanges();
                            //logger.Trace("已创建该用户信息...\n{0}\n{1}", request.Url, Newtonsoft.Json.JsonConvert.SerializeObject(user));
                        }
                        else
                        {
                            logger.Trace("用户信息已存在...\n{0}\n{1}", request.Url, Newtonsoft.Json.JsonConvert.SerializeObject(user));
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("{0}\n{1}\n{2}", request.Url, ex.Message, ex.ToString());
                    filterContext.Result = new ContentResult() { Content = "授权出错：" + code + "     具体错误：" + ex.Message };
                    return;
                }
                try
                {
                    if (!HttpContext.Current.Items.Contains(WeiChatApplicationContext.UserSessionName))
                        HttpContext.Current.Items.Add(WeiChatApplicationContext.UserSessionName, user);
                    HttpContext.Current.Session[WeiChatApplicationContext.UserSessionName] = user;
                    logger.Trace("已成功赋值...\n{0}", request.Url);
                }
                catch (Exception ex)
                {
                    logger.Error("{0}\n{1}\n{2}", request.Url, ex.Message, ex.ToString());
                    filterContext.Result = new ContentResult() { Content = "授权出错：" + code + "     具体错误：" + ex.Message };
                    return;
                }
                var redirectUrlCookie = request.Cookies[RedirectUrlCookieName];
                if (redirectUrlCookie != null)
                {
                    var redirectUrl = redirectUrlCookie.Value;
                    if (httpContextBase.Response.Cookies[RedirectUrlCookieName] != null)
                        httpContextBase.Response.Cookies.Remove(RedirectUrlCookieName);
                    filterContext.Result = new RedirectResult(redirectUrl);
                }
                #endregion
            }
            #endregion
            #region 如果没有验证，则进行验证
            else if (openIdCookie == null || string.IsNullOrEmpty(openIdCookie.Value))
            {
                var redirectUrl = request.Url.ToString();

                var cookie = new HttpCookie(RedirectUrlCookieName);
                cookie.Value = redirectUrl;
                httpContextBase.Response.Cookies.Add(cookie);

                var url = OAuthApi.GetAuthorizeUrl(WeiChatApplicationContext.Current.AppId, redirectUrl, _state, OAuthScope.snsapi_userinfo);
                logger.Trace("跳转至微信服务器获取授权...\n{0}\n{1}", redirectUrl, url);
                filterContext.Result = new RedirectResult(url);
            }
            #endregion
            #region 如果已验证
            else if (openIdCookie != null && !string.IsNullOrEmpty(openIdCookie.Value))
            {
                var openId = openIdCookie.Value;
                logger.Trace("从openIdCookie获取openId...{0}", openId);
                using (var db = new AppDbContext())
                {
                    var user = db.WeiChat_Users.FirstOrDefault(p => p.OpenId == openId);
                    if (user != null)
                    {
                        HttpContext.Current.Items.Add(WeiChatApplicationContext.UserSessionName, user);
                        HttpContext.Current.Session[WeiChatApplicationContext.UserSessionName] = user;
                    }
                    else
                    {
                        var redirectUrl = request.Url.ToString();
                        var cookie = new HttpCookie(RedirectUrlCookieName);
                        cookie.Value = redirectUrl;
                        httpContextBase.Response.Cookies.Add(cookie);

                        var url = OAuthApi.GetAuthorizeUrl(WeiChatApplicationContext.Current.AppId, redirectUrl, _state, OAuthScope.snsapi_userinfo);
                        logger.Trace("跳转至微信服务器获取授权...\n{0}\n{1}", redirectUrl, url);
                        //注意修改授权回调页面域名
                        filterContext.Result = new RedirectResult(url);
                    }
                }
            }
            #endregion
            else
            {
                logger.Error("授权出错，请检查...{0}", request.Url);
                filterContext.Result = new ContentResult() { Content = "授权出错，请检查！" };
            }
        }
    }
}
