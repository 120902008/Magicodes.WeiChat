﻿using Magicodes.WeiChat.Data.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public Configuration()
        {
            ContextKey = "Magicodes.WeiChat.Models.ApplicationDbContext";
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ////关闭自动生成迁移（让程序只打我们自己生成的迁移）
            //AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AppDbContext context)
        {
            var tenant = context.Account_Tenants.FirstOrDefault(p => p.IsSystemTenant);
            if (tenant == null)
            {
                tenant = new Account_Tenant() { IsSystemTenant = true, Name = "系统租户", Remark = "系统租户，勿删！" };
                context.Account_Tenants.Add(tenant);
                context.SaveChanges();
            }

            #region 添加用户和角色
            //全局管理员多租户Id为1
            var store = new AppUserStore(context) { TenantId = tenant.Id };
            var userManager = new UserManager<AppUser, string>(store);
            var roleManager = new RoleManager<AppRole>(new AppRoleStore(context));
            var adminRole = new AppRole() { Id = "{74ABBD8D-ED32-4C3A-9B2A-EB134BFF5D91}", Name = "Admin",Description= "超级管理员，拥有最大权限" };
            if (!roleManager.RoleExists(adminRole.Name))
                roleManager.Create(adminRole);
            var user = new AppUser
            {
                Id = "{B0FBB2AC-3174-4E5A-B772-98CF776BD4B9}",
                UserName = "admin",
                Email = "liwq@magicodes.net",
                EmailConfirmed = true,
                TenantId = tenant.Id
            };
            if (!userManager.Users.Any(p => p.Id == user.Id))
            {
                var result = userManager.Create(user, "123456abcD");
                if (result.Succeeded)
                {
                    userManager.AddToRole(user.Id, adminRole.Name);
                }
            }
            #endregion
        }
    }
}
