﻿using System;
using System.Collections.Generic;

namespace Magicodes.WeiChat.Data.BatchOperation
{
    public class TPHConfiguration
    {
        public Dictionary<Type, string> Mappings { get; set; }
        public string ColumnName { get; set; }

    }
}