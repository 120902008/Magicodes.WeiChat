﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.BatchOperation
{
    internal class BatchOperationHelper<TContext, T>
        where T : class
        where TContext : DbContext
    {
        private DbContext dbContext;
        private IDbSet<T> set;
        public BatchOperationHelper(TContext context, IDbSet<T> set)
        {
            this.dbContext = context;
            this.set = set;
        }
        public void BathInsert()
        {
            var con = dbContext.Database.Connection as EntityConnection;
            if (con != null)
            {
                var connectionToUse = con.StoreConnection;
                var currentType = typeof(T);
                var provider = Configuration.Providers.FirstOrDefault(p => p.CanHandle(connectionToUse));
                if (provider != null && provider.CanInsert)
                {
                    var mapping = EfMappingFactory.GetMappingsForContext(this.dbContext);
                    var typeMapping = mapping.TypeMappings[typeof(T)];
                    var tableMapping = typeMapping.TableMappings.First();

                    var properties = tableMapping.PropertyMappings
                        .Where(p => currentType.IsSubclassOf(p.ForEntityType) || p.ForEntityType == currentType)
                        .Select(p => new ColumnMapping { NameInDatabase = p.ColumnName, NameOnObject = p.PropertyName }).ToList();
                    if (tableMapping.TPHConfiguration != null)
                    {
                        properties.Add(new ColumnMapping
                        {
                            NameInDatabase = tableMapping.TPHConfiguration.ColumnName,
                            StaticValue = tableMapping.TPHConfiguration.Mappings[typeof(T)]
                        });
                    }

                    //provider.InsertItems(items, tableMapping.Schema, tableMapping.TableName, properties, connectionToUse, batchSize);
                }
                else
                {
                    Configuration.Log("Found provider: " + (provider == null ? "[]" : provider.GetType().Name) + " for " + connectionToUse.GetType().Name);
                    //Fallbacks.DefaultInsertAll(context, items);
                }
            }
        }
    }
}
