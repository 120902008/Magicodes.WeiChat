﻿namespace Magicodes.WeiChat.Data.BatchOperation
{
    public class ColumnMapping
    {
        public string NameOnObject { get; set; }
        public string StaticValue { get; set; }
        public string NameInDatabase { get; set; }

        public string DataType { get; set; }

        public bool IsPrimaryKey { get; set; }
    }
}