using System;

namespace Magicodes.WeiChat.Data.Models.Interface
{
    public interface IAdminUpdate<TUpdateByKey>
    {
        /// <summary>
        /// 创建人
        /// </summary>
        TUpdateByKey UpdateBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime? UpdateTime { get; set; }
    }
}