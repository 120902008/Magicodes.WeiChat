﻿namespace Magicodes.WeiChat.Data.Models.Interface
{
    public interface ITenantId
    {
        /// <summary>
        /// 租户Id
        /// </summary>
        int TenantId { get; set; }
    }
}