﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Magicodes.WeiChat.Data.Models.Interface;
using Magicodes.WeiChat.Framework;

namespace Magicodes.WeiChat.Data.Models
{
    public abstract class WeiChat_AdminUniqueTenantBase<TKey> : IAdminCreate<string>, IAdminUpdate<string>, ITenantId
    {
        [Key]
        public TKey Id { get; set; }

        [MaxLength(128)]
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        [MaxLength(128)]
        [Display(Name = "最后编辑")]
        public string UpdateBy { get; set; }
        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 租户Id
        /// </summary>
        [Index(IsUnique = true)]
        public int TenantId { get; set; }
    }
    /// <summary>
    /// App基类
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public abstract class WeiChat_TenantBase<TKey> : ITenantId, IAdminCreate<string>, IAdminUpdate<string>
    {
        [Key]
        public virtual TKey Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [MaxLength(128)]
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Display(Name = "创建者")]
        //[NotMapped]
        [ForeignKey("CreateBy")]
        public AppUser CreateUser { get; set; }

        /// <summary>
        /// 更新者
        /// </summary>
        [MaxLength(128)]
        public string UpdateBy { get; set; }
        /// <summary>
        /// 编辑者
        /// </summary>
        [MaxLength(256)]
        [Display(Name = "最后编辑")]
        //[NotMapped]
        public AppUser UpdateUser { get; set; }

        public int TenantId { get; set; }
    }
    /// <summary>
    /// 微信公众号配置
    /// </summary>
    public class WeiChat_App : IWeiChatConfig, IAdminCreate<string>, IAdminUpdate<string>, ITenantId
    {
        /// <summary>
        /// AppId
        /// </summary>
        [MaxLength(50)]
        [Required]
        [Display(Name = "AppId")]
        public string AppId { get; set; }
        /// <summary>
        /// AppSecret
        /// </summary>
        [Required]
        [MaxLength(100)]
        [Display(Name = "AppSecret")]
        [DataType(DataType.Password)]
        public string AppSecret { get; set; }
        /// <summary>
        /// 微信号
        /// </summary>
        [MaxLength(20)]
        [Display(Name = "微信号")]
        public string WeiXinAccount { get; set; }
        /// <summary>
        /// 版权信息
        /// </summary>
        [MaxLength(20)]
        [Display(Name = "版权信息")]
        public string CopyrightInformation { get; set; }
        /// <summary>
        /// 客户信息
        /// </summary>
        [MaxLength(20)]
        [Display(Name = "客户信息")]
        public string CustomerInformation { get; set; }
        /// <summary>
        /// 填写服务器配置时必须，为了安全，请生成自己的Token。注意：正式公众号的Token只允许英文或数字的组合，长度为3-32字符
        /// </summary>
        [MaxLength(200)]
        public string Token { get; set; }


        [MaxLength(128)]
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        [MaxLength(128)]
        [Display(Name = "最后编辑")]
        public string UpdateBy { get; set; }
        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 租户Id（使用租户Id作为Key）
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TenantId { get; set; }
        /// <summary>
        /// 公众号类型
        /// </summary>
        [Display(Name = "公众号类型")]
        public WeChatAppTypes WeChatAppType { get; set; }
    }
    /// <summary>
    /// 公众号类型
    /// </summary>
    public enum WeChatAppTypes
    {
        [Display(Name = "认证服务号")]
        Service = 0,
        [Display(Name = "认证订阅号")]
        Subscription = 1,
        [Display(Name = "企业号")]
        Enterprise = 2,
        [Display(Name = "测试号")]
        Test = 3
    }


    /// <summary>
    /// 同步日志
    /// </summary>
    public class WeiChat_SyncLog : ITenantId, IAdminCreate<string>
    {
        public WeiChat_SyncLog()
        {
            CreateTime = DateTime.Now;
        }
        /// <summary>
        /// 主键Id
        /// </summary>
        [Key]
        [Display(Name = "主键Id")]
        public int Id { get; set; }
        public WeiChat_SyncTypes Type { get; set; }
        public int TenantId { get; set; }
        /// <summary>
        /// 是否用户手动同步
        /// </summary>
        public bool IsUserSync { get; set; }
        [MaxLength(128)]
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }
        [MaxLength(500)]
        public string Message { get; set; }
        [Display(Name = "是否成功")]
        public bool Success { get; set; }
    }
    /// <summary>
    /// 多客服信息
    /// </summary>
    public class WeiChat_KFCInfo : WeiChat_TenantBase<int>
    {
        /// <summary>
        /// 客服账号
        /// </summary>
        [Display(Name = "客服账号")]
        public string Account { get; set; }

        /// <summary>
        /// 客服昵称
        /// </summary>
        [Display(Name = "客服昵称")]
        public string NickName { get; set; }

        /// <summary>
        /// 客服工号
        /// </summary>
        [Display(Name = "客服工号")]
        public string JobNumber { get; set; }

        /// <summary>
        /// 客服头像
        /// </summary>
        [Display(Name = "客服头像")]
        public string HeadImgUrl { get; set; }
    }
    public enum WeiChat_SyncTypes
    {
        /// <summary>
        /// 微信用户
        /// </summary>
        Sync_WeiChat_User = 0,
        /// <summary>
        /// 多客服信息
        /// </summary>
        Sync_MKF = 1,
        /// <summary>
        /// 图片资源
        /// </summary>
        Sync_Images = 2,
        /// <summary>
        /// 同步用户组
        /// </summary>
        Sync_WeiChat_UserGroup = 3,
        /// <summary>
        /// 同步模板消息
        /// </summary>
        Sync_MessagesTemplates = 4
    }

    public class WeiChat_Material
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public DateTime UpdateTime { get; set; }
    }

}