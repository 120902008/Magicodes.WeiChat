﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Framework
{
    /// <summary>
    /// 接口参数异常
    /// </summary>
    public class ApiArgumentException : ArgumentException
    {
        public ApiArgumentException() : base() { }
        public ApiArgumentException(string message) : base(message)
        {

        }
        public ApiArgumentException(string message,string paramName) : base(message, paramName) { }
    }
}
