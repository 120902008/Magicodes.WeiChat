﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Framework
{
    /// <summary>
    /// 微信配置信息
    /// </summary>
    public interface IWeiChatConfig
    {
        string AppId { get; set; }
        string AppSecret { get; set; }
        string WeiXinAccount { get; set; }
        string Token { get; set; }
    }
}
