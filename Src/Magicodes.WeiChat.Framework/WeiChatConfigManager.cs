﻿using Magicodes.WeiChat.Unity;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Helpers;
using System;
using System.Collections.Concurrent;
using System.Web;
using System.Web.Configuration;

namespace Magicodes.WeiChat.Framework
{

    /// <summary>
    /// 微信配置管理对象
    /// </summary>
    public class WeiChatConfigManager : ThreadSafeLazyBaseSingleton<WeiChatConfigManager>
    {
        protected ConcurrentDictionary<object, IWeiChatConfig> WeiChatConfigs = new ConcurrentDictionary<object, IWeiChatConfig>();
        const string KEYNAME = "WeiChatConfigManager_Key";
        /// <summary>
        /// 设置配置Key，以便获取相关配置
        /// </summary>
        /// <param name="key"></param>
        public void SetKey(object key)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new HttpException("SetKey只能在当前请求线程中使用！");
            }
            context.Session[KEYNAME] = key;
        }
        public object GetKey()
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new HttpException("GetKey只能在当前请求线程中使用！");
            }
            return context.Session[KEYNAME];
        }
        public IWeiChatConfig GetConfig()
        {
            return GetConfig(GetKey());
        }
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="key">唯一Key</param>
        /// <returns></returns>
        public IWeiChatConfig GetConfig(object key)
        {
            if (key == null)
            {
                throw new Exception("Key不能为NULL！");
            }
            if (WeiChatConfigs.ContainsKey(key))
            {
                return WeiChatConfigs[key];
            }
            else
            {
                var result = WeiChatFrameworkFuncsManager.Current.InvokeFunc(WeiChatFrameworkFuncTypes.Config_GetWeiChatConfigByKey, key);
                if (result != null)
                {
                    var weiChatConfig = result as IWeiChatConfig;
                    WeiChatConfigs.AddOrUpdate(key, weiChatConfig, (tKey, existingVal) => { return weiChatConfig; });
                    return weiChatConfig;
                }
                else
                {
                    throw new Exception(string.Format("通过Key：{0}获取Config失败！", key));
                }
            }
        }
        /// <summary>
        /// 获取当前页面JS配置信息
        /// </summary>
        /// <returns></returns>
        public JSSDKConfigInfo GetJSSDKConfigInfo()
        {
            var ticket = AccessTokenContainer.TryGetJsApiTicket(AppId, AppSecret);
            var configInfo = new JSSDKConfigInfo()
            {
                AppId = AppId,
                Timestamp = JSSDKHelper.GetTimestamp(),
                NonceStr = JSSDKHelper.GetNoncestr()
            };
            configInfo.Signature = JSSDKHelper.GetSignature(ticket, configInfo.NonceStr, configInfo.Timestamp, HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            return configInfo;
        }
        /// <summary>
        /// 微信AppId
        /// </summary>
        public string AppId
        {
            get
            {
                return GetConfig().AppId;
            }
        }
        /// <summary>
        /// 接口访问密钥
        /// </summary>
        public string AppSecret
        {
            get
            {
                return GetConfig().AppSecret;
            }
        }
        /// <summary>
        /// 微信号
        /// </summary>
        public string WeiXinAccount
        {
            get
            {
                return GetConfig().WeiXinAccount;
            }
        }

        /// <summary>
        /// 接口访问凭据
        /// </summary>
        public string AccessToken
        {
            get
            {
                var config = GetConfig();
                return AccessTokenContainer.TryGetAccessToken(config.AppId, config.AppSecret);
            }
        }
        /// <summary>
        /// 接口访问凭据
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="appSecret"></param>
        /// <returns></returns>
        public string GetAccessToken(string appId, string appSecret)
        {
            return AccessTokenContainer.TryGetAccessToken(appId, appSecret);
        }
        /// <summary>
        /// 刷新访问凭据
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="appSecret"></param>
        public void RefreshAccessToken(string appId, string appSecret)
        {
            AccessTokenContainer.TryGetAccessToken(appId, appSecret, true);
        }
        /// <summary>
        /// 刷新配置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="config"></param>
        public void RefreshConfig(object key, IWeiChatConfig config)
        {
            WeiChatConfigs.AddOrUpdate(key, config, (tKey, existingVal) => { return config; });
        }
        /// <summary>
        /// 刷新配置以及访问凭据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="config"></param>
        public void RefreshConfigAndAccessToken(object key, IWeiChatConfig config)
        {
            RefreshConfig(key, config);
            RefreshAccessToken(config.AppId, config.AppSecret);
        }
    }
}
