﻿using Magicodes.WeiChat.Framework.Apis.Material;
using Magicodes.WeiChat.Framework.Apis.QRCode;
using Magicodes.WeiChat.Framework.Apis.TemplateMessage;
using Magicodes.WeiChat.Framework.Apis.User;
using Magicodes.WeiChat.Framework.Apis.UserGroup;
using System;
using Magicodes.WeiChat.Unity;
using Magicodes.WeiChat.Framework.Apis.Statistics;
using Magicodes.WeiChat.Framework.Apis.CustomerService;
using Magicodes.WeiChat.Framework.Apis.Menu;

namespace Magicodes.WeiChat.Framework
{
    /// <summary>
    /// 接口上下文对象
    /// </summary>
    public class WeiChatApisContext: ThreadSafeLazyBaseSingleton<WeiChatApisContext>
    {
        /// <summary>
        /// 微信二维码
        /// </summary>
        public QRCodeApi QrCodeApi = new QRCodeApi();
        /// <summary>
        /// 模板消息
        /// </summary>
        public TemplateMessageApi TemplateMessageApi = new TemplateMessageApi();
        /// <summary>
        /// 用户组相关操作API
        /// </summary>
        public UserGroupApi UserGroupApi = new UserGroupApi();
        /// <summary>
        /// 用户相关操作
        /// </summary>
        public UserApi UserApi = new UserApi();
        /// <summary>
        /// 多图文接口
        /// </summary>
        public NewsApi NewsApi = new NewsApi();
        /// <summary>
        /// 数据统计API
        /// </summary>
        public StatisticsApi StatisticsApi = new StatisticsApi();
        /// <summary>
        /// 多客服接口
        /// </summary>
        public CustomerServiceApi CustomerServiceApi = new CustomerServiceApi();
        /// <summary>
        /// 自定义菜单接口
        /// </summary>
        public MenuApi MenuApi = new MenuApi();
    }
}
