﻿using Magicodes.WeiChat.Unity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Framework
{
    /// <summary>
    /// 方法类型
    /// </summary>
    public enum WeiChatFrameworkFuncTypes
    {
        /// <summary>
        /// 根据Key获取微信配置
        /// </summary>
        Config_GetWeiChatConfigByKey = 0,
        /// <summary>
        /// 模板消息发送
        /// </summary>
        APIFunc_TemplateMessageApi_Create = 1,
        /// <summary>
        /// 创建二维码
        /// </summary>
        APIFunc_QRCodeApi_Create = 2
    }
    /// <summary>
    /// 函数管理器
    /// </summary>
    public class WeiChatFrameworkFuncsManager : ThreadSafeLazyBaseSingleton<WeiChatFrameworkFuncsManager>
    {
        /// <summary>
        /// 函数集合
        /// </summary>
        internal ConcurrentDictionary<WeiChatFrameworkFuncTypes, Func<object, object>> Funcs = new ConcurrentDictionary<WeiChatFrameworkFuncTypes, Func<object, object>>();
        /// <summary>
        /// 注册函数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="func"></param>
        public void Register(WeiChatFrameworkFuncTypes eventType, Func<object, object> func)
        {
            if (Funcs.ContainsKey(eventType))
            {
                throw new Exception(string.Format("{0}已经注册，不能重复注册！", eventType));
            }
            Funcs.AddOrUpdate(eventType, func, (tKey, existingVal) =>
            {
                return func;
            });
        }
        /// <summary>
        /// 获取函数
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public Func<object, object> GetFunc(WeiChatFrameworkFuncTypes eventType)
        {
            if (Funcs.ContainsKey(eventType))
            {
                return Funcs[eventType];
            }
            return null;
        }
        /// <summary>
        /// 执行函数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="obj"></param>
        public object InvokeFunc(WeiChatFrameworkFuncTypes eventType, object obj)
        {
            var func = GetFunc(eventType);
            if (func != null)
            {
                //TODO:修改为异步处理
                return func.Invoke(obj);
            }
            return null;
        }
    }
}
