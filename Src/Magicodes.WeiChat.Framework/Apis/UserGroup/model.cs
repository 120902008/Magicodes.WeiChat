﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Framework.Apis.UserGroup
{
    /// <summary>
    /// 用户组创建模型
    /// </summary>
    public class UserGroupCreateModel
    {
        /// <summary>
        /// 用户组
        /// </summary>
        [JsonProperty("group")]
        public UserGroupInfo Group { get; set; }
        /// <summary>
        /// 用户组信息
        /// </summary>
        public class UserGroupInfo
        {
            /// <summary>
            /// 组名
            /// </summary>
            [MaxLength(30)]
            [Required]
            [JsonProperty("name")]
            public string Name { get; set; }
        }
    }
    /// <summary>
    /// 用户组创建结果模型
    /// </summary>
    public class UserGroupCreateResultModel : ApiResult
    {
        /// <summary>
        /// 用户组
        /// </summary>
        [JsonProperty("group")]
        public UserGroupInfo Group { get; set; }
        /// <summary>
        /// 用户组信息
        /// </summary>
        public class UserGroupInfo
        {
            /// <summary>
            /// 分组id，由微信分配
            /// </summary>
            [JsonProperty("id")]
            public int Id { get; set; }
            /// <summary>
            /// 分组名字，UTF8编码
            /// </summary>
            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public override bool IsSuccess()
        {
            return this.Message == null;
        }
    }
    /// <summary>
    /// 用户组获取结果模型
    /// </summary>
    public class UserGroupGetResultModel : ApiResult
    {
        /// <summary>
        /// 用户组
        /// </summary>
        [JsonProperty("groups")]
        public List<UserGroupInfo> Groups { get; set; }
        /// <summary>
        /// 用户组信息
        /// </summary>
        public class UserGroupInfo
        {
            /// <summary>
            /// 分组id，由微信分配
            /// </summary>
            [JsonProperty("id")]
            public int Id { get; set; }
            /// <summary>
            /// 分组名字，UTF8编码
            /// </summary>
            [JsonProperty("name")]
            public string Name { get; set; }
            /// <summary>
            /// 分组内用户数量
            /// </summary>
            [JsonProperty("count")]
            public int Count { get; set; }
        }
        public override bool IsSuccess()
        {
            return this.Message == null;
        }
    }

    /// <summary>
    /// 用户组获取结果模型
    /// </summary>
    public class UserGroupGetByIdResultModel : ApiResult
    {
        /// <summary>
        /// 用户组Id
        /// </summary>
        [JsonProperty("groupid")]
        public int GroupId { get; set; }

        public override bool IsSuccess()
        {
            return this.Message == null;
        }
    }
}
