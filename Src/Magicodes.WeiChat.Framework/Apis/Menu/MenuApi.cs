﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Framework.Apis.Menu
{
    /// <summary>
    /// 自定义菜单接口
    /// http://mp.weixin.qq.com/wiki/10/0234e39a2025342c17a7d23595c6b40a.html
    /// </summary>
    public class MenuApi : ApiBase
    {
        const string APIName = "menu";
        /// <summary>
        /// 自定义菜单查询接口
        /// https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
        /// </summary>
        /// <returns>菜单返回结果</returns>
        public MenuGetApiResultModel Get()
        {
            //获取api请求url
            var url = GetAccessApiUrl("get", APIName);
            return Get<MenuGetApiResultModel>(url, new MenuButtonsCustomConverter());
        }
    }
}
