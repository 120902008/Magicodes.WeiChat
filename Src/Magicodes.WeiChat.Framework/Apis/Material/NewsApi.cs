﻿using System;
using System.Web;

namespace Magicodes.WeiChat.Framework.Apis.Material
{
    /// <summary>
    /// 多图文接口
    /// </summary>
    public class NewsApi : ApiBase
    {
        const string APIName = "material";

        /// <summary>
        /// 获取多图文素材
        /// </summary>
        /// <param name="id">要获取的素材的media_id</param>
        /// <returns>图文消息结果</returns>
        public NewsGetApiResult Get(string id)
        {
            //获取api请求url
            var url = GetAccessApiUrl("get_material", APIName);
            var data = new
            {
                media_id = id
            };
            return Post<NewsGetApiResult>(url, data);
        }
        public ApiResult Get(int offset = 0, int count = 20)
        {
            //获取api请求url
            var url = GetAccessApiUrl("batchget_material", APIName);
            var data = new
            {
                type = "news",
                offset = offset,
                count = count
            };
            return Post<ApiResult>(url, data);
        }


        public NewsPostApiResult Post(NewsPostModel news)
        {
            //获取api请求url
            var url = GetAccessApiUrl("add_news", APIName);
            //foreach (var item in news.Articles)
            //{
            //    item.Content = HttpUtility.HtmlEncode(HttpUtility.UrlEncode(item.Content));
            //}
            return Post<NewsPostApiResult>(url, news, (inputStr) =>
            {
                return inputStr;
            });
        }
    }
}
