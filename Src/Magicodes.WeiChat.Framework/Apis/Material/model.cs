﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Magicodes.WeiChat.Framework.Apis.Material
{
    /// <summary>
    /// 多图文素材获取结果
    /// </summary>
    public class NewsGetApiResult : ApiResult
    {
        /// <summary>
        /// 文章列表
        /// </summary>
        [JsonProperty("news_item")]
        public List<ArticleInfo> Items { get; set; }
        /// <summary>
        /// 文章信息
        /// </summary>
        public class ArticleInfo
        {
            /// <summary>
            /// 图文消息的标题
            /// </summary>
            [JsonProperty("title")]
            public string Title { get; set; }
            /// <summary>
            /// 图文消息的封面图片素材id（必须是永久mediaID）
            /// </summary>
            [JsonProperty("thumb_media_id")]
            public string ThumbMediaId { get; set; }
            /// <summary>
            /// 是否显示封面，0为false，即不显示，1为true，即显示
            /// </summary>
            [JsonProperty("show_cover_pic")]
            public int ShowCoverPic { get; set; }
            /// <summary>
            /// 作者
            /// </summary>
            [JsonProperty("author")]
            public string Author { get; set; }
            /// <summary>
            /// 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
            /// </summary>
            [JsonProperty("digest")]
            public string Digest { get; set; }
            /// <summary>
            /// 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
            /// </summary>
            [JsonProperty("content")]
            public string Content { get; set; }
            /// <summary>
            /// 图文页的URL
            /// </summary>
            [JsonProperty("url")]
            public string Url { get; set; }
            /// <summary>
            /// 图文消息的原文地址，即点击“阅读原文”后的URL
            /// </summary>
            [JsonProperty("content_source_url")]
            public string ContentSourceUrl { get; set; }
        }

    }
    /// <summary>
    /// 新增的图文消息素材的media_id
    /// </summary>
    public class NewsPostApiResult : ApiResult
    {
        [JsonProperty("media_id")]
        public string MediaId { get; set; }
        public override bool IsSuccess()
        {
            return base.IsSuccess() && !string.IsNullOrWhiteSpace(MediaId);
        }
    }
    public class NewsPostModel
    {
        /// <summary>
        /// 文章列表
        /// </summary>
        [JsonProperty("articles")]
        public List<ArticleInfo> Articles { get; set; }
        /// <summary>
        /// 文章信息
        /// </summary>
        public class ArticleInfo
        {
            /// <summary>
            /// 图文消息的标题
            /// </summary>
            [JsonProperty("title")]
            public string Title { get; set; }
            /// <summary>
            /// 图文消息的封面图片素材id（必须是永久mediaID）
            /// </summary>
            [JsonProperty("thumb_media_id")]
            public string ThumbMediaId { get; set; }
            /// <summary>
            /// 是否显示封面，0为false，即不显示，1为true，即显示
            /// </summary>
            [JsonProperty("show_cover_pic")]
            public int ShowCoverPic { get; set; }
            /// <summary>
            /// 作者
            /// </summary>
            [JsonProperty("author")]
            public string Author { get; set; }
            /// <summary>
            /// 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
            /// </summary>
            [JsonProperty("digest")]
            public string Digest { get; set; }
            /// <summary>
            /// 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
            /// </summary>
            [JsonProperty("content")]
            public string Content { get; set; }

            /// <summary>
            /// 图文消息的原文地址，即点击“阅读原文”后的URL
            /// </summary>
            [JsonProperty("content_source_url")]
            public string ContentSourceUrl { get; set; }
        }
    }




}
