﻿using Magicodes.WeiChat.Controllers;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Framework.Apis.Menu;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Magicodes.WeiChat.Framework.Test.ApiTests
{
    [TestClass]
    public class MenusApiTest : ApiTestBase
    {
        MenuApi weChatApi = new MenuApi();
        public MenusApiTest()
        {
            weChatApi.SetKey(1);
        }

        [TestMethod]
        public void MenusApiTest_Get()
        {
            var result = weChatApi.Get();
            if (!result.IsSuccess())
            {
                Assert.Fail("获取菜单数据失败，返回结果如下：" + result.DetailResult);
            }
        }
    }
}
