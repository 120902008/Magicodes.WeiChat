﻿using Magicodes.WeiChat.Controllers;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Framework.Apis.CustomerService;
using Magicodes.WeiChat.Framework.Apis.Menu;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Magicodes.WeiChat.Framework.Test.ApiTests
{
    [TestClass]
    public class CustomerServiceApiTest : ApiTestBase
    {
        CustomerServiceApi weChatApi = new CustomerServiceApi();
        public CustomerServiceApiTest()
        {
            weChatApi.SetKey(1);
        }

        [TestMethod]
        public void CustomerServiceApiTest_GetCustomerAccountList()
        {
            var result = weChatApi.GetCustomerAccountList();
            if (!result.IsSuccess())
            {
                Assert.Fail("获取多客服账号列表失败，返回结果如下：" + result.DetailResult);
            }
        }

        [TestMethod]
        public void CustomerServiceApiTest_CRUDCustomerAccount()
        {
            var testAccountName = "Test";
            weChatApi.RemoveCustomerAccount(testAccountName);
            var result = weChatApi.AddCustomerAccount(testAccountName, "Test", "111111");
            if (!result.IsSuccess())
            {
                Assert.Fail("创建客服账号失败，返回结果如下：" + result.DetailResult);
            }
            else
            {
                result = weChatApi.UpdateCustomerAccount(testAccountName, "TestUpdate", "222222");
                if (!result.IsSuccess())
                {
                    Assert.Fail("修改客服账号失败，返回结果如下：" + result.DetailResult);
                }
                result = weChatApi.RemoveCustomerAccount(testAccountName);
                if (!result.IsSuccess())
                {
                    Assert.Fail("删除客服账号失败，返回结果如下：" + result.DetailResult);
                }
            }
        }
    }
}
