﻿using Magicodes.WeiChat.Controllers;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Framework.Apis.Menu;
using Magicodes.WeiChat.Framework.Apis.QRCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Magicodes.WeiChat.Framework.Test.ApiTests
{
    [TestClass]
    public class QRCodeApiTest : ApiTestBase
    {
        QRCodeApi weChatApi = new QRCodeApi();
        public QRCodeApiTest()
        {
            weChatApi.SetKey(1);
        }

        [TestMethod]
        public void QRCodeApiTest_CreateByNumberValue()
        {
            var result = weChatApi.CreateByNumberValue(new Random().Next(1, 100000));
            if (!result.IsSuccess())
            {
                Assert.Fail("创建二维码失败，返回结果如下：" + result.DetailResult);
            }
        }
    }
}
