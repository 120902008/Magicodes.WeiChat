﻿using Magicodes.WeiChat.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Framework.Test
{
    [TestClass]
    public class ApiTestBase
    {
        static ApiTestBase()
        {
            //注册获取配置函数：根据Key获取微信配置（加载一次后续将缓存）
            WeiChatFrameworkFuncsManager.Current.Register(WeiChatFrameworkFuncTypes.Config_GetWeiChatConfigByKey,
                (key) =>
                {
                    using (AppDbContext _db = new AppDbContext())
                    {
                        var appConfig = _db.WeiChat_Apps.Find(key);
                        if (appConfig == null)
                        {
                            throw new Exception("您尚未配置公众号，请配置公众号信息！");
                        }
                        return appConfig;
                    }
                });
        }
        protected AppDbContext db = new AppDbContext();
        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        /// <summary>
        /// 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Initialize(TestContext testContext)
        {
           
        }

        /// <summary>
        /// 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup() { }

        /// <summary>
        /// 在运行每个测试之前，使用 TestInitialize 来运行代码
        /// </summary>
        [TestInitialize()]
        public void TestInitialize() { }
        /// <summary>
        /// 在每个测试运行完之后，使用 TestCleanup 来运行代码
        /// </summary>
        [TestCleanup()]
        public void TestCleanup() { }
    }
}
