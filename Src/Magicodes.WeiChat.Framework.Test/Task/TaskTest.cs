﻿using Magicodes.WeiChat.WeChatHelper.Task;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Framework.Test.Task
{
    [TestClass]
    public class TaskTest : ApiTestBase
    {
        SyncHelper helper = new SyncHelper(2);

        [TestMethod]
        public void Sync_WeiChat_User()
        {
            Assert.IsTrue(helper.Sync(Data.Models.WeiChat_SyncTypes.Sync_WeiChat_User).Result);
        }

    }
}
