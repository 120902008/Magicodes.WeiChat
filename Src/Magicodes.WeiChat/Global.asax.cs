﻿using Magicodes.WeiChat.App_Start;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.WeChatHelper.Task;
using System.Web.SessionState;

namespace Magicodes.WeiChat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        Logger log = LogManager.GetCurrentClassLogger();
        protected void Application_Start()
        {
            //记录托管异常
            AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;

            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            TaskManager.Current.StartAllTasks();
        }
        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }
        private bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            //获取异常
            var lastError = Server.GetLastError().GetBaseException();
            string requestStr = null;
            if (HttpContext.Current != null)
            {
                var context = HttpContext.Current;
                requestStr = string.Format("URL:{1}{0}", Environment.NewLine, context.Request.Url);
            }
            if (string.IsNullOrEmpty(requestStr))
                log.Error(lastError);
            else
                log.Error(lastError, requestStr);
        }

        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            //LogManager.GetCurrentClassLogger().Error(e.Exception, "托管代码错误");
            //if (e.Exception.InnerException != null)
            //{
            //    LogManager.GetCurrentClassLogger().Error(e.Exception.InnerException, "托管代码错误");
            //}
        }
    }

}
