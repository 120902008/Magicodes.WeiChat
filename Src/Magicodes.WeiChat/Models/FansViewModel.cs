﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Models
{
    public class RemarkViewModel
    {
        public string OpenId { get; set; }
        public string Remark { get; set; }
    }
}