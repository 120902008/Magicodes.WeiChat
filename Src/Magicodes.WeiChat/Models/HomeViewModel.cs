﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Models
{
    public class HomeUserViewModel
    {
        public string NickName { get; set; }
        public string SubscribeTime { get; set; }
        public string Style { get; set; }
    }
    public class HomeChartViewModel
    {
        public string X { get; set; }
        public string Y { get; set; }
    }
}