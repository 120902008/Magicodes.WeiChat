﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Models
{
    public class CreateAdminUserViewModel
    {
        [Required]
        [Display(Name = "用户名")]
        [MaxLength(20)]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "确认密码")]
        [Compare("Password", ErrorMessage = "确认密码不匹配！")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "手机号码")]
        [MaxLength(11)]
        public string PhoneNumber { get; set; }
    }
}