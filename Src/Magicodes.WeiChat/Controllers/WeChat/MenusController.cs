﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Ajax;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Entities.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    public class MenusController : TenantBaseController<WeiChat_App>
    {
        // GET: Menus
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            var result = CommonApi.GetMenu(WeiChatConfigManager.Current.AccessToken);
            if (result != null && result.menu != null && result.menu.button != null)
            {
                return Json(result.menu.button, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new List<string>(), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPut]
        public ActionResult Put(GetMenuResultFull resultFull)
        {
            var ajaxResponse = new AjaxResponse() { Success = true, Message = "同步成功！" };
            //重新整理按钮信息
            var bg = CommonApi.GetMenuFromJsonResult(resultFull, new ButtonGroup()).menu;
            var result = CommonApi.CreateMenu(WeiChatConfigManager.Current.AccessToken, bg);
            if (result.errcode != ReturnCode.请求成功)
            {
                return Json(result.errmsg);
            }
            return Json(ajaxResponse);
        }
    }
}