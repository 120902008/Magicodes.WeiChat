﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Models;
using Senparc.Weixin.MP.AdvancedAPIs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using Senparc.Weixin.MP.AdvancedAPIs;
namespace Magicodes.WeiChat.Controllers.WeChat
{
    public class WeiChatUsersController : TenantBaseController<WeiChat_User>
    {
        //
        // GET: /WeiChatUsers/
        public ActionResult Index(int pageIndex = 1, int pageSize = 12, int? groupId = null)
        {
            var q = db.WeiChat_Users.AsQueryable();
            if (groupId != null)
            {
                q = q.Where(p => p.GroupId == groupId);
            }
            var pagedList = new PagedList<WeiChat_User>(
                 q
                 .OrderByDescending(p => p.SubscribeTime)
                 .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(),
                 pageIndex, pageSize, q.Count());
            return View(pagedList);
        }

        [HttpPost]
        public ActionResult Remark(RemarkViewModel model)
        {
            var message = new MessageInfo()
            {
                Message = "操作成功！",
                MessageType = MessageTypes.Success
            };
            var user = db.WeiChat_Users.FirstOrDefault(p => p.OpenId == model.OpenId);
            if (user != null)
            {
                UserApi.UpdateRemark(AccessToken, model.OpenId, model.Remark);
                //TODO:判断是否成功
                user.Remark = model.Remark;
                db.SaveChanges();
            }
            else
            {
                message.Message = "账号不存在！";
                message.MessageType = MessageTypes.Danger;
            }
            return Json(message);
        }
    }
}