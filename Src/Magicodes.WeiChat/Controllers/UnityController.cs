﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Infrastructure.Cache;
using Magicodes.WeiChat.Models;
using Magicodes.WeiChat.Unity;
using Magicodes.WeiChat.WeChatHelper.Task;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.Analysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    [Authorize]
    public class UnityController : TenantBaseController<WeiChat.Data.Models.WeiChat_App>
    {
        /// <summary>
        /// 获取系统管理员页面链接
        /// </summary>
        /// <returns></returns>
        public ActionResult GetSystemAdminHomeHtml()
        {
            return Content(IsSystemIenant ? string.Format("<a href='{0}'>系统管理员界面</a>",Url.Action("Index", "AdminHome",new { area= "SystemAdmin" })) : "");
        }
    }
}