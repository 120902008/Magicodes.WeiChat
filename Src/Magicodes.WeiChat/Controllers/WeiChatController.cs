﻿using Senparc.Weixin.MP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using Magicodes.WeiChat.App_Start;
using System.IO;
using NLog;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using Magicodes.WeiChat.Unity;
using Senparc.Weixin;
using System.Web.Configuration;
using Magicodes.WeiChat.Framework;
using Senparc.Weixin.MP.AdvancedAPIs;

namespace Magicodes.WeiChat.Controllers
{
    [RoutePrefix("WeiChat")]
    public class WeiChatController : BaseController
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 填写服务器配置时必须，为了安全，请生成自己的Token。注意：正式公众号的Token只允许英文或数字的组合，长度为3-32字符
        /// </summary>
        [Route("{tenantId}")]
        [AllowAnonymous]
        public ActionResult Index(int tenantId, string signature, string timestamp, string nonce, string echostr)
        {
            var token = WeiChatConfigManager.Current.GetConfig(tenantId).Token;
            //get method - 仅在微信后台填写URL验证时触发
            if (CheckSignature.Check(signature, timestamp, nonce, token))
            {
                return Content(echostr); //返回随机字符串则表示验证通过
            }
            else
            {
                return Content("failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, token) + "。" +
                           "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。");
            }
        }

        [HttpPost]
        [Route("{tenantId}")]
        [AllowAnonymous]
        public ActionResult Create(int tenantId, string signature, string timestamp, string nonce, string echostr)
        {
            try
            {
                var token = WeiChatConfigManager.Current.GetConfig(tenantId).Token;
                //post method - 当有用户想公众账号发送消息时触发
                if (!CheckSignature.Check(signature, timestamp, nonce, token))
                {
                    logger.Error("服务器事件转发错误，请检查Token是否正确！");
                    return Content("参数错误，请检查Token是否正确！");
                }
                //设置每个人上下文消息储存的最大数量，防止内存占用过多，如果该参数小于等于0，则不限制
                var maxRecordCount = 10;

                //自定义MessageHandler，对微信请求的详细判断操作都在这里面。
                var messageHandler = new MessageHandler(Request.InputStream, tenantId, maxRecordCount);

#if DEBUG
                //测试时可开启此记录，帮助跟踪数据，使用前请确保App_Data文件夹存在，且有读写权限。
                messageHandler.RequestDocument.Save(
                    Server.MapPath("~/App_Data/" + DateTime.Now.Ticks + "_Request_" +
                                   messageHandler.RequestMessage.FromUserName + ".txt"));
#endif

                try
                {
                    //执行微信处理过程
                    messageHandler.Execute();
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Error(ex.InnerException.Message);
                    if (messageHandler.ResponseDocument != null)
                    {
                        logger.Error(messageHandler.ResponseDocument.ToString());
                    }
                }
#if DEBUG
                //测试时可开启，帮助跟踪数据
                messageHandler.ResponseDocument.Save(
                   Server.MapPath("~/App_Data/" + DateTime.Now.Ticks + "_Response_" +
                                   messageHandler.ResponseMessage.ToUserName + ".txt"));

#endif

                if (messageHandler.ResponseDocument == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                return Content(messageHandler.ResponseDocument.ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException.Message);
                return Content(ex.Message);
            }
        }
        /// <summary>
        /// 获取验证信息
        /// </summary>
        /// <param name="redirectUrl"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("OAuth")]
        [AllowAnonymous]
        public ActionResult OAuth(string redirectUrl)
        {
            if (string.IsNullOrWhiteSpace(redirectUrl))
            {
                return Content("请传递参数redirectUrl");
            }
#if DEBUG
            //var logger = LogManager.GetCurrentClassLogger();
            //logger.Debug("OAuth redirectUrl：" + redirectUrl);
#endif
            //使用state存放目标url地址与参数
            //var url = OAuthApi.GetAuthorizeUrl(WeixinHelper.appId, ConfigHelper.Domain + "/WeiChat/OAuth/Code", redirectUrl, OAuthScope.snsapi_userinfo);
            var url = OAuthApi.GetAuthorizeUrl(WeiChatConfigManager.Current.AppId, redirectUrl, "magicodes.weichat", OAuthScope.snsapi_userinfo);
#if DEBUG
            //logger.Debug("OAuth url：" + url);
#endif
            return Redirect(url);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("OAuth/Code")]
        public ActionResult OAuthCode(string code, string state)
        {
#if DEBUG
            //var logger = LogManager.GetCurrentClassLogger();
            //logger.Debug("OAuth/Code：\n\r\tcode=" + code + "\n\r\tstate:" + state);
#endif

            if (string.IsNullOrEmpty(code))
            {
                return Content("您拒绝了授权！");
            }
            //通过，用code换取access_token
            var result = OAuthApi.GetAccessToken(WeiChatConfigManager.Current.AppId, WeiChatConfigManager.Current.AppSecret, code);
            if (result.errcode != ReturnCode.请求成功)
            {
                return Content("错误：" + result.errmsg);
            }
            var url = Server.UrlDecode(state);
            if (string.IsNullOrWhiteSpace(url))
            {
                return Content("验证失败！请从正规途径进入！");
            }
            else
            {
                //已关注，可以得到详细信息
                //userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                url += url.EndsWith("&") ? string.Empty : "&";
                url += "access_token=" + result.access_token + "&openid=" + result.openid;
                return Redirect(url);
            }
            //下面2个数据也可以自己封装成一个类，储存在数据库中（建议结合缓存）
            //如果可以确保安全，可以将access_token存入用户的cookie中，每一个人的access_token是不一样的
            //Session["OAuthAccessTokenStartTime"] = DateTime.Now;
            //Session["OAuthAccessToken"] = result;
        }
    }
}
