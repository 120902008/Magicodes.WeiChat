﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Infrastructure.Cache;
using Magicodes.WeiChat.Models;
using Magicodes.WeiChat.Unity;
using Magicodes.WeiChat.WeChatHelper.Task;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.Analysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    [Authorize]
    public class HomeController : TenantBaseController<WeiChat.Data.Models.WeiChat_App>
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            //如果没有配置租户信息，则必须前去配置
            if (!HasConfigWeiChat && IsSystemIenant)
            {
                return RedirectToAction("WeiChatAppConfig", "Account_Tenant", new { area = "SystemAdmin", tenantId = TenantId });
            }
            #region 获取最新关注的5位用户
            var last5Users = GetLast5Users();
            //if (last5Users != null || last5Users.Count == 0)
            //{
            //    //如果不存在用户，则触发同步逻辑
            //    var syncHelper = new SyncHelper(TenantId);
            //    syncHelper.Sync(WeiChat_SyncTypes.Sync_WeiChat_User, false, UserId);
            //}
            ViewBag.Last5Users = last5Users;
            #endregion

            #region 半年公众号关注变化
            var chartData = new List<HomeChartViewModel>();
            for (int i = 0; i < 6; i++)
            {
                var dataItem = new HomeChartViewModel();
                var currentTime = DateTime.Now.AddMonths(-i);
                dataItem.X = currentTime.ToString("MM月");
                dataItem.Y = db.WeiChat_Users.Where(p => p.SubscribeTime.Year == currentTime.Year && p.SubscribeTime.Month == currentTime.Month).Count().ToString();
                chartData.Add(dataItem);
            }
            ViewBag.X = "'" + string.Join("','", chartData.Select(p => p.X).ToArray()) + "'";
            ViewBag.Y = string.Join(",", chartData.Select(p => p.Y).ToArray());
            ViewBag.ChartData = chartData;
            #endregion

            var cache = Magicodes.WeiChat.Infrastructure.Cache.CacheManager.Current;
            #region 累积关注数
            {
                var value = cache.GetByTenant<int>("UserSummaryCount");
                if (value == default(int))
                {
                    value = SafeReturnHelper.SafeReturn<int>(new Func<int>(
                        () =>
                        {
                            var userCumulateData = AnalysisApi.GetUserCumulate(WeiChatConfigManager.Current.AccessToken, DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd"), DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));

                            if (userCumulateData.list != null && userCumulateData.list.Count > 0)
                            {
                                return Convert.ToInt32(userCumulateData.list.First().cumulate_user);
                            }
                            return default(int);
                        }
                    ));
                }
                cache.AddOrUpdateByTenant("UserSummaryCount", value, TimeSpan.FromHours(12));
                ViewBag.UserSummaryCount = value;
            }

            #endregion


            #region 取消关注数、新关注数、净增用户数
            {
                //取消关注数
                var cancelUserCount = cache.GetByTenant<int>("CancelUserCount");
                //新关注数
                var newUserCount = cache.GetByTenant<int>("NewUserCount");
                //净增用户数
                var growUserCount = cache.GetByTenant<int>("GrowUserCount");
                if (newUserCount == default(int))
                {
                    AnalysisResultJson<UserSummaryItem> value = SafeReturnHelper.SafeReturn<AnalysisResultJson<UserSummaryItem>>(new Func<AnalysisResultJson<UserSummaryItem>>(
                        () =>
                        {
                            return AnalysisApi.GetUserSummary(WeiChatConfigManager.Current.AccessToken, DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd"), DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
                        }
                    ));
                    if (value != null)
                    {
                        cancelUserCount = value.list.Select(p => Convert.ToInt32(p.cancel_user)).Sum();
                        newUserCount = value.list.Select(p => Convert.ToInt32(p.new_user)).Sum();
                        growUserCount = Convert.ToInt32(newUserCount) - Convert.ToInt32(cancelUserCount);
                        cache.AddOrUpdateByTenant("CancelUserCount", cancelUserCount, TimeSpan.FromHours(12));
                        cache.AddOrUpdateByTenant("NewUserCount", newUserCount, TimeSpan.FromHours(12));
                        cache.AddOrUpdateByTenant("GrowUserCount", growUserCount, TimeSpan.FromHours(12));
                    }
                }
                //取消关注数
                ViewBag.CancelUserCount = cancelUserCount;
                //新关注数
                ViewBag.NewUserCount = newUserCount;
                //净增用户数
                ViewBag.GrowUserCount = growUserCount;
            }

            #endregion


            #region 粉丝分布
            var provinces = db.WeiChat_Users.Where(p => p.Subscribe).Select(p => p.Province).Distinct().ToList();
            ViewBag.ProvinceStr = "'" + string.Join("','", provinces) + "'";
            var valueList = new List<int>();
            foreach (var item in provinces)
            {
                valueList.Add(db.WeiChat_Users.Where(p => p.Subscribe && p.Province == item).Count());
            }
            ViewBag.ProvinceDataStr = "" + string.Join(",", valueList) + "";
            #endregion
            return View();
        }

        private List<HomeUserViewModel> GetLast5Users()
        {
            var styles = new string[] { "success", "info", "primary", "default", "primary" };
            var last5Users = db.WeiChat_Users.Where(p => p.Subscribe).OrderByDescending(p => p.SubscribeTime).Take(5).ToList().Select(p => new HomeUserViewModel
            {
                NickName = p.NickName,
                SubscribeTime = p.SubscribeTime.ToString("MM-dd HH:mm:ss"),
            }).ToList();
            for (int i = 0; i < last5Users.Count; i++)
            {
                last5Users[i].Style = styles[i];
            }
            return last5Users;
        }

        /// <summary>
        /// 500
        /// </summary>
        /// <returns></returns>
        [Route("Error")]
        public ActionResult Error()
        {
            return View();
        }
        /// <summary>
        /// 404
        /// </summary>
        /// <returns></returns>
        [Route("NotFoundError")]
        public ActionResult NotFoundError()
        {
            return View();
        }
    }
}