﻿using EntityFramework.DynamicFilters;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models.Interface;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Results;
using Magicodes.WeiChat.Infrastructure.Tenant;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    [Authorize]
    public class TenantBaseController<TEntry> : BaseController
        where TEntry : class, ITenantId, new()
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //设置配置Key
            WeiChatConfigManager.Current.SetKey(TenantId);
            TenantManager.Current.EnableTenantFilter(db, TenantId);
            base.OnActionExecuting(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            TenantManager.Current.DisableTenantFilter(db);
        }
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return base.Json(data, contentType, contentEncoding, behavior);
        }
        /// <summary>
        /// 导出CSV
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>

        public ActionResult Csv(IEnumerable<TEntry> data)
        {
            return new CsvFileResult<TEntry>(data);
        }
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Excel(IEnumerable<TEntry> data)
        {
            return new ExcelFileResult<TEntry>(data);
        }

    }
}