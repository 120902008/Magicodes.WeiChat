﻿using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Magicodes.WeiChat.Controllers.WebApi
{
    [RoutePrefix("api/WeiChatUser")]
    public class WeiChatUserApiController : WebApiControllerBase
    {
        protected AppDbContext db = new AppDbContext();
        [Route("")]
        // GET: api/WeiChatUser
        public async Task<IHttpActionResult> Get(string key, int top = 100)
        {
            var queryable = db.WeiChat_Users.Where(p => p.Subscribe && p.TenantId == WeiChatApplicationContext.Current.TenantId).AsQueryable();
            if (!string.IsNullOrWhiteSpace(key))
            {
                queryable = queryable.Where(p => p.NickName.Contains(key));
            }
            return Ok(queryable.Take(top).ToList());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
