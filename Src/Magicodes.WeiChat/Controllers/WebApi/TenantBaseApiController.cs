﻿using EntityFramework.DynamicFilters;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models.Interface;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Results;
using Magicodes.WeiChat.Infrastructure.Tenant;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers.WebApi
{
    [Authorize]
    public class TenantBaseApiController<TEntry> : WebApiControllerBase
        where TEntry : class, ITenantId, new()
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            var tenantId = TenantId;
            //设置配置Key
            WeiChatConfigManager.Current.SetKey(tenantId);
            TenantManager.Current.EnableTenantFilter(db, tenantId);
            base.Initialize(controllerContext);
        }
    }
}