﻿using Magicodes.WeiChat.Infrastructure.MvcExtension.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Magicodes.WeiChat.Infrastructure;

namespace Magicodes.WeiChat.Areas.App.Controllers
{
    //注意继承自：AppBaseController
    public class AppDemoController : AppBaseController
    {
        // GET: App/AppDemo/WeChatOAuthTest
        //博客说明：http://www.cnblogs.com/codelove/p/5355514.html
        [WeChatOAuth]
        public ActionResult WeChatOAuthTest()
        {
            return View(WeiChatUser);
        }
    }
}