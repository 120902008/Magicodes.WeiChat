﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Areas.App.Controllers
{
    public class AppController : AppBaseController
    {
        // GET: App/App
        public ActionResult Success(string msgTitle = "操作成功", string message = "", string href = null, string hrefTitle = "查看详情", string okUrl = null, string data = null)
        {
            ViewBag.msgTitle = msgTitle;
            ViewBag.message = message;
            ViewBag.href = href;
            ViewBag.hrefTitle = hrefTitle;
            ViewBag.okUrl = okUrl;
            return View();
        }
    }
}