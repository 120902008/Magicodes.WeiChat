﻿using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Magicodes.WeiChat.Areas.App.Controllers
{
    [AllowAnonymous]
    public class AppBaseController : Controller
    {
        internal const string TenantIdSessionName = "Magicodes.TenantId";
        protected AppDbContext db = new AppDbContext();
        /// <summary>
        /// 租户Id
        /// </summary>
        public int TenantId
        {
            get
            {
                //请求参数中的租户Id
                var reqTennantId = default(int);
                #region 获取请求参数中的租户Id
                if (!string.IsNullOrWhiteSpace(Request.QueryString["TenantId"]))
                {
                    reqTennantId = Convert.ToInt32(Request.QueryString["TenantId"]);
                }
                else
                {
                    reqTennantId = Request.RequestContext.RouteData.Values["TenantId"] != null ? Convert.ToInt32(Request.RequestContext.RouteData.Values["TenantId"]) : default(int);
                }
                #endregion
                if (reqTennantId != default(int))
                {
                    HttpContext.Session[TenantIdSessionName] = reqTennantId;
                    return reqTennantId;
                }
                else if (HttpContext.Session[TenantIdSessionName] != null)
                {
                    return Convert.ToInt32(HttpContext.Session[TenantIdSessionName]);
                }
                return default(int);
            }
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //设置配置Key
            WeiChatConfigManager.Current.SetKey(TenantId);
            base.OnActionExecuting(filterContext);
        }
        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            //设置配置Key
            WeiChatConfigManager.Current.SetKey(TenantId);
            base.OnAuthentication(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
        /// <summary>
        /// 微信用户信息
        /// </summary>
        public WeiChat_User WeiChatUser { get { return WeiChatApplicationContext.Current.WeiChatUser; } }
    }
}