﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Infrastructure.Identity;
using Magicodes.WeiChat.Models;
using Magicodes.WeiChat.Unity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web.Configuration;

namespace Magicodes.WeiChat.Areas.SystemAdmin.Controllers
{
    [RouteArea("SystemAdmin")]
    [RoutePrefix("Login")]
    [Route("{action}")]
    public class LoginController : Controller
    {
        private AppDbContext db = new AppDbContext();
        public LoginController()
        {
            UserManager = new UserManager<AppUser, string>(new AppUserStore(new AppDbContext())
            {
                TenantId = db.Account_Tenants.First(p => p.IsSystemTenant).Id
            });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "Login");
        }

        public UserManager<AppUser, string> UserManager { get; private set; }

        [AllowAnonymous]
        [Route("")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Title = string.Format("{0} | 系统登录", WebConfigurationManager.AppSettings["CustomerInformation"]);
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("")]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    try
                    {
                        await SignInAsync(user, model.RememberMe);
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                        return View(model);
                    }
                    return RedirectToAction("Index", "AdminHome");
                }
                else
                {
                    ModelState.AddModelError("", "用户名或密码错误！");
                }
            }
            return View(model);
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(AppUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

    }
}