﻿using Magicodes.WeiChat.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Areas.SystemAdmin.Controllers
{
    public class AdminHomeController : SystemAdminBase<WeiChat_App, int>
    {
        // GET: SystemAdmin/AdminHome
        public ActionResult Index()
        {
            return View();
        }
    }
}