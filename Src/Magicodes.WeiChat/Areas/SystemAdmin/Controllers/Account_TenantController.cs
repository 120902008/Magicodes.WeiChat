﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Ajax;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Magicodes.WeiChat.Controllers;
using Webdiyer.WebControls.Mvc;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.Interface;
using Magicodes.WeiChat.Framework;
using Magicodes.WeiChat.WeChatHelper.Task;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Areas.SystemAdmin.Controllers
{
    public class Account_TenantController : SystemAdminBase<WeiChat_App, int>
    {

        // GET: SystemAdmin/Account_Tenant1
        public ActionResult Index(string q, int pageIndex = 1, int pageSize = 10)
        {
            var queryable = db.Account_Tenants.AsQueryable();
            if (!string.IsNullOrWhiteSpace(q))
            {
                //请替换为相应的搜索逻辑
                queryable = queryable.Where(p => p.Name.Contains(q) || p.Remark.Contains(q));
            }
            var pagedList = new PagedList<Account_Tenant>(
                             queryable.OrderBy(p => p.Id)
                             .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(),
                             pageIndex, pageSize, queryable.Count());
            return View(pagedList);
        }

        // GET: SystemAdmin/Account_Tenant1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_Tenant account_Tenant = db.Account_Tenants.Find(id);
            if (account_Tenant == null)
            {
                return HttpNotFound();
            }
            return View(account_Tenant);
        }

        // GET: SystemAdmin/Account_Tenant1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SystemAdmin/Account_Tenant1/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IsSystemTenant,Name,Remark")] Account_Tenant account_Tenant)
        {
            if (ModelState.IsValid)
            {
                db.Account_Tenants.Add(account_Tenant);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(account_Tenant);
        }

        // GET: SystemAdmin/Account_Tenant1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_Tenant account_Tenant = db.Account_Tenants.Find(id);
            if (account_Tenant == null)
            {
                return HttpNotFound();
            }
            return View(account_Tenant);
        }

        // POST: SystemAdmin/Account_Tenant1/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IsSystemTenant,Name,Remark")] Account_Tenant account_Tenant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(account_Tenant).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(account_Tenant);
        }

        // GET: SystemAdmin/Account_Tenant1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_Tenant account_Tenant = db.Account_Tenants.Find(id);
            if (account_Tenant == null)
            {
                return HttpNotFound();
            }
            if (account_Tenant.IsSystemTenant)
            {
                throw new Exception("系统租户无法删除！");
            }
            return View(account_Tenant);
        }

        // POST: SystemAdmin/Account_Tenant1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            Account_Tenant account_Tenant = db.Account_Tenants.Find(id);
            db.Account_Tenants.Remove(account_Tenant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: SystemAdmin/Account_Tenant1/BatchOperation/{operation}
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="operation">操作方法</param>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SystemAdmin/Account_Tenant/BatchOperation/{operation}")]
        public ActionResult BatchOperation(string operation, params int?[] ids)
        {
            var ajaxResponse = new AjaxResponse();
            if (ids.Length > 0)
            {
                try
                {
                    var models = db.Account_Tenants.Where(p => ids.Contains(p.Id)).ToList();
                    if (models.Count == 0)
                    {
                        ajaxResponse.Success = false;
                        ajaxResponse.Message = "没有找到匹配的项，项已被删除或不存在！";
                        return Json(ajaxResponse);
                    }
                    switch (operation.ToUpper())
                    {
                        case "DELETE":
                            #region 删除
                            {
                                db.Account_Tenants.RemoveRange(models);
                                db.SaveChanges();
                                ajaxResponse.Success = true;
                                ajaxResponse.Message = string.Format("已成功操作{0}项！", models.Count);
                                break;
                            }
                        #endregion
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    ajaxResponse.Success = false;
                    ajaxResponse.Message = ex.Message;
                }
            }
            else
            {
                ajaxResponse.Success = false;
                ajaxResponse.Message = "请至少选择一项！";
            }
            return Json(ajaxResponse);
        }

        public ActionResult WeiChatAppConfig(int tenantId)
        {
            var model = db.WeiChat_Apps.FirstOrDefault(p => p.TenantId == tenantId) ?? new WeiChat_App()
            {
                TenantId = tenantId,
                Token = Guid.NewGuid().ToString("N")
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> WeiChatAppConfig(WeiChat_App model)
        {
            if (!ModelState.IsValid) return View(model);
            var isAdd = !db.WeiChat_Apps.Any(p => p.TenantId == model.TenantId);
            try
            {
                SaveModel(model, isAdd, model.TenantId);
                WeiChatConfigManager.Current.RefreshConfigAndAccessToken(model.TenantId, model);
                db.SaveChanges();
                var syncHelper = new SyncHelper(model.TenantId);
                if (Request.Form["Sync_WeiChat_UserGroup"] == "1")
                    await syncHelper.Sync(WeiChat_SyncTypes.Sync_WeiChat_UserGroup, true, UserId);
                if (Request.Form["Sync_WeiChat_User"] == "1")
                    await syncHelper.Sync(WeiChat_SyncTypes.Sync_WeiChat_User, true, UserId);
                if (Request.Form["Sync_MKF"] == "1")
                    await syncHelper.Sync(WeiChat_SyncTypes.Sync_MKF, true, UserId);
                if (Request.Form["Sync_MessagesTemplates"] == "1")
                    await syncHelper.Sync(WeiChat_SyncTypes.Sync_MessagesTemplates, true, UserId);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "配置信息有误，请检查配置！");
                return View(model);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
