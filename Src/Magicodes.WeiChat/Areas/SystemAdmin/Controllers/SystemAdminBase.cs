﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using Magicodes.WeiChat.Controllers;
using Magicodes.WeiChat.Data.Models.Interface;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using Microsoft.AspNet.Identity;

namespace Magicodes.WeiChat.Areas.SystemAdmin.Controllers
{
    public class SystemAdminBase<TModel, Tkey> : BaseController
        where TModel : class, IAdminCreate<string>, IAdminUpdate<string>, ITenantId
    {
        protected void SaveModel(TModel model, bool isAdd, int? tenantId)
        {
            //判断是否为默认值
            if (isAdd)
            {
                model.CreateBy = UserId;
                model.CreateTime = DateTime.Now;
                model.TenantId = tenantId == null ? UserTenantId : tenantId.Value;
                db.Set<TModel>().Add(model);
            }
            else
            {
                db.Set<TModel>().Attach(model);
                //取数据库值
                var databaseValues = db.Entry(model).GetDatabaseValues();
                model.CreateBy = databaseValues.GetValue<string>("CreateBy");
                model.CreateTime = databaseValues.GetValue<DateTime>("CreateTime");
                model.UpdateTime = DateTime.Now;
                model.UpdateBy = UserId;
                db.Entry(model).State = EntityState.Modified;
            }
        }

        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                filterContext.HttpContext.Response.Redirect("/SystemAdmin/Login");

            var userId = User.Identity.GetUserId();
            var tenantId = db.Users.Find(userId).TenantId;
            if (!db.Account_Tenants.Any(p => p.Id == tenantId && p.IsSystemTenant))
            {
                filterContext.HttpContext.Response.Redirect("/SystemAdmin/Login");
            }
            base.OnAuthentication(filterContext);
        }
    }
}