﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Helpers.UEEditor
{
    public class CrawlerHelper
    {
        private string[] Sources;
        private Crawler[] Crawlers;
        public object Crawle()
        {
            Sources = HttpContext.Current.Request.Form.GetValues("source[]");
            if (Sources == null || Sources.Length == 0)
            {
                return (new
                {
                    state = "参数错误：没有指定抓取源"
                });
            }
            Crawlers = Sources.Select(x => new Crawler(x, HttpContext.Current.Server).Fetch()).ToArray();
            return (new
            {
                state = "SUCCESS",
                list = Crawlers.Select(x => new
                {
                    state = x.State,
                    source = x.SourceUrl,
                    url = x.ServerUrl
                })
            });
        }
    }
}