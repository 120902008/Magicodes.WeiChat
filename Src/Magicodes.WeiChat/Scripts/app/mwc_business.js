﻿var mwc = mwc || {};
mwc.bs = mwc.bs || {};
(function () {
    mwc.bs.init = function (setting) {
        var defaultSetting = {
            //是否启用ICheck
            iCheck: true,
            //是否启用选择所有
            checkAll: true,
            //是否启用模式框链接触发
            modalClick: true,
            //是否启用批量操作
            batchOperation: true,
            //是否启用loadingButton
            loadingButton: false
        };
        var options = $.extend(defaultSetting, setting);
        if (options.iCheck) {
            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });
        }
        if (options.checkAll) {
            $("#chkCheckAll").on('ifToggled', function (e) {
                $("input[type=checkbox][name=ids]").iCheck(e.currentTarget.checked ? "check" : "uncheck");
            });
        }
        if (options.modalClick) {
            //将clicktype='modal'属性的链接均用弹窗打开
            $("a[data-clicktype='modal']").on('click', function () {
                var url = $(this).data('url');
                var title = $(this).data('title');
                mwc.window.show(title, url);
            });
        }
        if (options.batchOperation) {
            //批量操作
            $("#toolBar button[data-action],a[data-action]").on('click', function () {
                var action = $(this).data("action") || "BatchOperation";
                mwc.bs.batchOperation(action, $(this).data("comfirmmessage"), $(this).data("param"), $(this).data("reloadparent"));
            });
        }
        if (options.loadingButton) {
            var $btns = $('button.loadingButton,a.loadingButton').ladda();
            if ($btns.ladda) {
                $btns.on('click', function () {
                    var $btn = $(this);
                    $btn.ladda('start');
                    var ajaxUrl = $btn.data('ajaxurl');
                    mwc.restApi.get({
                        url: ajaxUrl,
                        isBlockUI: false,
                        success: function () {
                            $btn.ladda('stop');
                            if ($btn.data('reload') === true) {
                                setTimeout(function () { window.location.reload(); }, 800);
                            }
                        },
                        error: function () {
                            $btn.ladda('stop');
                        }
                    });

                });
            } else {
                console.error('请在页面添加LoadingButton的相关脚本资源！');
            }
        }
    }

    mwc.bs.postBatchOperation = function (operation, controller) {
        var ids = new Array();
        $.each($("input[type=checkbox][name=ids]:checked").serializeArray(), function (i, v) {
            ids.push(v.value);
        });
        var apiData = { ids: ids };
        if (param)
            apiData.param = param;
        var url = controller ? (controller + '/BatchOperation/' + operation) : ('BatchOperation/' + operation);
        mwc.restApi.post({
            url: url,
            contentType: "application/x-www-form-urlencoded",
            data: apiData,
            success: function (data) {
                if (reloadParent)
                    parent.location.reload();
                else
                    location.reload();
            }
        });
    }

    mwc.bs.batchOperation = function (operation, comfirmMessage, param, reloadParent) {
        var $checkInputs = $("input[type=checkbox][name=ids]:checked");
        if ($checkInputs.length === 0) {
            mwc.message.warn("请至少选择一项！");
            return;
        }
        if (comfirmMessage) {
            mwc.message.confirm("", comfirmMessage, function (isConfirmed) {
                isConfirmed && mwc.bs.postBatchOperation(operation);
            });
        } else {
            mwc.bs.postBatchOperation(operation);
        }
    }


})();