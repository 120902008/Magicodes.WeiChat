﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Framework.Apis.User;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.BatchOperation;
using Magicodes.WeiChat.Infrastructure.Tenant;

namespace Magicodes.WeiChat.Test.EFBatchOperationTest
{
    /// <summary>
    /// EFBatchOperationTest 的摘要说明
    /// </summary>
    [TestClass]
    public class EFBatchOperationTest
    {
        AppDbContext db = new AppDbContext();
        AppDbContext opDb = new AppDbContext();
        public EFBatchOperationTest()
        {
            //
            //TODO:  在此处添加构造函数逻辑
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        //
        // 编写测试时，可以使用以下附加特性: 
        //
        // 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {

        }
        //
        // 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        [ClassCleanup()]
        public static void MyClassCleanup() {
            
        }
        //
        // 在运行每个测试之前，使用 TestInitialize 来运行代码
        [TestInitialize()]
        public void MyTestInitialize()
        {
            InsertTestRows();
        }

        private void InsertTestRows()
        {
            //插入10万行数据
            var testData = GetTestData();
            db.BathInsert(db.WeiChat_Users, testData);
        }

        //
        // 在每个测试运行完之后，使用 TestCleanup 来运行代码
        [TestCleanup()]
        public void MyTestCleanup()
        {
            db.Database.ExecuteSqlCommand("Delete from MWC.WeiChat_User");
            //TenantManager.Current.DisableTenantFilter(db);
            //db.BathRemoveBy(db.WeiChat_Users, p => true);
        }
        #endregion

        [TestMethod]
        public void TestEFInsert()
        {
            var data = GetTestData();
            {
                db.WeiChat_Users.AddRange(data);
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void TestEFInsertOptimization()
        {
            var data = GetTestData();
            {
                opDb.Configuration.AutoDetectChangesEnabled = false;
                opDb.Configuration.ValidateOnSaveEnabled = false;

                opDb.WeiChat_Users.AddRange(data);
                opDb.SaveChanges();
            }
        }

        [TestMethod]
        public void TestBathInsert()
        {
            var data = GetTestData();
            {
                db.BathInsert(db.WeiChat_Users, data);
            }
        }

        [TestMethod]
        public void TestBathUpdate()
        {
            db.BathUpdateBy(db.WeiChat_Users, p => p.Sex == WeChatSexTypes.Man, p => p.Sex, p => WeChatSexTypes.Woman);
        }

        private static List<WeiChat_User> GetTestData()
        {
            var list = new List<WeiChat_User>();
            for (int i = 0; i < 100000; i++)
            {
                list.Add(new WeiChat_User()
                {
                    AllowTest = true,
                    City = "上海",
                    Country = "China",
                    Language = "婴语",
                    NickName = "雪雁",
                    OpenId = Guid.NewGuid().ToString(),
                    Province = "上海",
                    Remark = "超级管理员",
                    Sex = WeChatSexTypes.Man,
                    Subscribe = true,
                    SubscribeTime = DateTime.Now,
                    TenantId = 1,
                });
            }
            return list;
        }
    }
}
