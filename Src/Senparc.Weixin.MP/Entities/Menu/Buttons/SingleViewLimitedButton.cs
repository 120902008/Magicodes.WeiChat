﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Senparc.Weixin.MP.Entities.Menu
{
    /// <summary>
    /// Url按钮
    /// </summary>
    public class SingleViewLimitedButton : SingleButton
    {
        /// <summary>
        /// 类型为view时必须
        /// 网页链接，用户点击按钮可打开链接，不超过256字节
        /// </summary>
        public string media_id { get; set; }

        public SingleViewLimitedButton()
            : base(ButtonType.view_limited.ToString())
        {
        }
    }
}
