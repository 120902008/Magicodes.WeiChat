﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WeiChat.Magicodes.Startup))]
namespace WeiChat.Magicodes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
