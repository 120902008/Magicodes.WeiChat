﻿Param (
    ##源目录
    [string]$src = "E:\WorkSpace\Magicodes.WeiChat\Src",
    [string]$import="E:\WorkSpace\Magicodes.WeiChat\Tools\Import\*",
    [string]$target="E:\WorkSpace\Magicodes.WeiChat\Publish",
    [string]$documentsDir="E:\OneDrive\文档\Products\Magicodes.WeiChat\Documents\*"
)

if(![io.Directory]::Exists($src))
{
    Write-Error "源目录不存在。";
    return;
}
#if(![io.Directory]::Exists($import))
#{
#    Write-Error "导入目录不存在。";
#    return;
#}
if(![io.Directory]::Exists($target))
{
    [io.Directory]::CreateDirectory($target)
}
$version = Read-Host "请输入版本号"
Write-Warning "版本号为：$version"
$targetDir=[io.Path]::Combine($target,"Magicodes.WeiChat_$version")
Write-Warning "目标目录为：$targetDir"
#创建目标目录
if(![io.Directory]::Exists($targetDir))
{
    [io.Directory]::CreateDirectory($targetDir)
    Write-Host "目标目录创建成功！"
}else
{
    Write-Error "目录存在，操作已取消。$targetDir";
    return;
}
#开始复制
Copy-Item -Path  $src  -Destination $targetDir  -Recurse  -Force
Write-Host "目录复制完成！"
#清理内容
dir $targetDir -Recurse | 
        Where-Object { ($_.Name -eq "bin") -or ($_.Name -eq "obj")  -or ($_.Name -eq "logs") -or ($_.Name -match "^*.ldf$")  -or ($_.Name -match "^*.mdf$") -or ($_.Name -eq "TestResults") -or ($_.Name -eq "MediaFiles") -or ($_.Name -eq "upload") -or ($_.Name -eq "PublishProfiles") }|
        ForEach-Object  {  
                            Write-Host $_.FullName;
                            remove-item -Path $_.FullName -Recurse -Force ;
                         } |
        Select-Object -Property Name
Write-Host "已成功清理相关内容！"
#导入文件
Write-Host "导入配置文件..."
Copy-Item -Path  $import  -Destination $targetDir  -Recurse  -Force
Write-Host "导入文档..."
Copy-Item -Path  $documentsDir  -Destination $targetDir  -Recurse  -Force
Write-Host "已成功导入相关文件！"



